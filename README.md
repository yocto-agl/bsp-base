Usage:

  source ./setup-environment machine-name build-dir

Current:

  DISTRO=poky-agl MACHINE=raspberrypi3 source ./setup-environment build/poky-agl-raspberrypi3 agl-image-ivi && agl-image-ivi-crosssdk

  DISTRO=poky-agl MACHINE=rpi3 source ./setup-environment build/poky-agl-rpi3 papillon-basic-image && papillon-basic-image-crosssdk



  DISTRO=poky-agl MACHINE=imx6qsabrelite source ./setup-environment build/poky-agl-imx6qsabrelite agl-image-ivi && agl-image-ivi-crosssdk
